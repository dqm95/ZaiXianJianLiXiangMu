<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.dqm.pojo.EducationExperience" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>修改教育经历</title>
</head>
<%EducationExperience education = (EducationExperience)request.getAttribute("edu"); %>
<%String message = (String)request.getAttribute("message"); %>
<%String[] educations = {"大专","学士","硕士","博士"}; %>
<script type="text/javascript" src="js/laydate.dev.js"></script>	
	<script type="text/javascript">
		laydate({
			elem: '#start_time'
		});   
		laydate({
			elem: '#end_time'
		});		
	</script>
<body>
	<div id="header">
	<%@include file="header.jsp" %>
	</div>
	<form action="updateEdu1?id=
	<%if(message != "null"){%>
	<%=education.getId() %><%} %>" method="post">
		<table width="100%" border=1>
			<%if(message == "null"){ %>
			<tr style="text-align:center"><td>修改的信息不能为空!</td></tr>
			<%} else{%>
			<tr>
				<td>学校</td>
				<td><input type="text" name="school" value=<%=education.getSchool() %> /></td>
			</tr>
			<tr>
				<td>开始时间</td>
				<td>
					<input type="text" name="startTime" onclick="laydate()" value=<%=education.getStartTime()%>/>
				</td>	
			</tr>
			<tr>
				<td>结束时间</td>
				<td>
					<input type="text" name="endTime" onclick="laydate()" value=<%=education.getEndTime()%>/>
				</td>
			</tr>
			<tr>
				<td>取得学位</td>
				<td>
					<select name="education">
					<%for(int i = 0; i < 4; i++) {%>
						<%if(education.getEducation() == educations[i]) {%>
					<option value=<%=i%> selected="selected"><%=educations[i]%></option>
						<%} else {%>
							<option value=<%=i%>><%=educations[i]%></option>
						<%} %>
					<%} %>
					</select>
				</td>
			</tr>
			<%} %>
		</table>
		<%if(message != "null") {%>
		<input type="submit" value="确定"/>
		<%} %>
		<a href="listResume"><input type="button" value="取消"></a>
	</form>
</body>
</html>