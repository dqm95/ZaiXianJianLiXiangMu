<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ page import="com.dqm.pojo.User" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>欢迎页面</title>
</head>
<%User user = (User)session.getAttribute("currentUser"); %>
<body>
	<form action="reset" method="post">
		<fieldset>
			<legend>重置账号信息</legend>
			账号:<%=user.getUsername()%><br><br>
			新密码:<input type="password" name="password"/><br><br>
			新昵称:<input type="text" name="nickname"/><br><br>
			<input type="submit" value="提交"/>&nbsp;&nbsp;&nbsp;
			<a href="index.jsp"><input type="button" value="取消"></a>
		</fieldset>
	</form>
	
</body>
</html>