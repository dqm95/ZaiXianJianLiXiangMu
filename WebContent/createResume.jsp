<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>添加简历页面</title>
<%String[] educations={"大专","学士","硕士","博士"};%>
<%String message = (String)request.getAttribute("message");%>
<script type="text/javascript" src="js/laydate.dev.js"></script>	
	<script type="text/javascript">
		laydate({
			elem: '#start_time'
		});   
		laydate({
			elem: '#end_time'
		});		
	</script>
</head>
<body>
	<div id="header">
	<%@include file="header.jsp" %>
	</div>
	<h2>欢迎光临君强科技有限公司简历注册！</h2>
	<form action="createResume" method="post">
		<fieldset>
			<legend>简历填写</legend>	
			<%if(message == "null"){ %>
			<span style="font-size:x-large;">填写的信息不能为空!</span><br><br>
			<%} %>
			姓名:<input type="text" name="name" placeholder="请输入姓名"/>
			<br><br>
			性别:<input type="radio" name="sex" value="true" checked="checked"/>男&nbsp;
			<input type="radio" name="sex" value="false"/>女
			<br><br>
			民族:<input type="text" name="nation"/>
			<br><br>
			出生日期:<input type="text" name="birthday" onclick="laydate()"/>
			<br><br>
			家庭住址:<input type="text" name="address"/>
			<br><br>
			现居地址：<input type="text" name="nowaddress"/>
			<br><br>
			政治面貌:<input type="text" name="political_status"/>
			<br><br>
			应聘职位:<input type="text" name="position"/>
			<br><br>
			专业:<input type="text" name="major"/>
			<br><br>
			最高学历:<select name="education_level">
			<%for(int i = 0; i < 4; i++) {%>
				<option value=<%=i%>><%=educations[i]%></option>
			<%} %>
				</select>
				<br>
				<br>
			<a href="user.jsp"><input type="button" value="上一步"></a>&nbsp;
			<input type="submit" value="下一步">
		</fieldset>
	</form>
</body>
</html>