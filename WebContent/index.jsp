<%@page import="org.springframework.ui.Model"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="com.dqm.pojo.User"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>欢迎页面</title>
</head>
<%User user = (User) request.getAttribute("user");%>
<%String message = (String) request.getAttribute("isLogin");%>
<body>
	<h2>欢迎光临君强科技有限公司简历注册！</h2>
	<%if (user == null) {%>
	<form action="login" method="post">
		<fieldset>
			<legend>登录</legend><br> 
			用户名:<input type="text" name="username"><br>
			密码:<input type="password" name="password"><br> 
				<input type="submit" value="登录">&nbsp;&nbsp; 
				<a href="signup.jsp"><input type="button" value="注册"></a> 
			<%if(message == "no"){ %>
			<span>用户或密码错误!</span>
			<%} else if(message == "empty"){%>
			<span>填写信息不能为空</span>
			<%} %>
			<%if(message =="reset"){ %>
			<span>账号信息已经重置，请重新登录</span>
			<%} %>
		</fieldset>
	</form>
	<%}%>
</body>
</html>