<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>添加教育经历</title>
</head>
<script type="text/javascript" src="js/laydate.dev.js"></script>	
	<script type="text/javascript">
		laydate({
			elem: '#start_time'
		});   
		laydate({
			elem: '#end_time'
		});		
	</script>
<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<%String[] educations = {"大专","学士","硕士","博士"}; %>
<script>
$(function(){  
	var a = 0;
    $("#getAtr").click(function(){  
    	var rows = document.getElementById("myTable").rows.length;
    	a++;
        $str='';
        $str+="<tr>";
        $str+="<td><input type='text' name=\"startTime"+a+"\" id=\"start_time"+a+"\" onClick='laydate()'/></td>";
        $str+="<td><input type='text' name=\"endTime"+a+"\" id=\"end_time"+a+"\" onClick='laydate()'/></td>";
        $str+="<td><input type='text' name=\"school"+a+"\" placeholder='请输入您就读的学校'/></td>";
        $str+="<td><select name=\"education"+a+"\"><%for(int i=0;i<4;i++){ %><option value=<%=educations[i]%>><%=educations[i] %></option><%} %></select></td>";
        $str+="<td  onclick='getDel(this)'><a href='#'>删除</a></td>";   
        $str+="</tr>";
        $("#addTr").append($str);  
    });
});
 
function getDel(k){
    $(k).parent().remove();     
}
</script>
<%String message = (String)request.getAttribute("message");%>
<body>
	<h2>欢迎光临君强科技有限公司简历注册！</h2>
	<form action="createEducation" method="post">
	<%if(message == "null"){ %>
		<span style="font-size:x-large;">填写的信息不能为空!</span><br><br>
	<%} %>
		<table id="myTable" border="1">
			<tr>
				<td colspan="5" align="center"><h2>添加教育经历</h2></td>
				<td><input type="button" id="getAtr" value="Add"><input type="submit" value="Next"/></td>
			</tr>
			<tr>
				<td><center>开始时间</center></td>
				<td><center>结束时间</center></td>
				<td><center>学校</center></td>
				<td><center>学历</center></td>
				<td colspan="2"><center>操作</center></td>
			</tr>
			<tbody id="addTr">
			<tr>
				<td><input type="text" name="startTime0" onclick="laydate();"/></td>
				<td><input type="text" name="endTime0" onClick="laydate();"/></td>
				<td><input type="text" name="school0" placeholder="请输入您就读的学校"/></td>
				<td><select name="education0">
				<%for(int i=0;i<4;i++){ %>
					<option value=<%=educations[i]%>><%=educations[i] %></option>
				<%} %>
			</select></td>
				<td onclick="getDel(this)"><a href='#'>删除</a></td>
			</tr>	
			</tbody>
		</table>
	</form>

</body>
</html>