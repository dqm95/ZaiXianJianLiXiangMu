<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.dqm.pojo.WorkExperience" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>修改工作经历</title>
</head>
<%WorkExperience work = (WorkExperience)request.getAttribute("work"); %>
<%String message = (String)request.getAttribute("message"); %>
<script type="text/javascript" src="js/laydate.dev.js"></script>	
	<script type="text/javascript">
		laydate({
			elem: '#start_time'
		});   
		laydate({
			elem: '#end_time'
		});		
	</script>
<body>
	<div id="header">
	<%@include file="header.jsp" %>
	</div>
	<form action="updateWork1?id=
	<%if(message != "null"){%>
	<%=work.getId() %><%} %>" method="post">
		<table width="100%" border=1>
			<%if(message == "null"){ %>
			<tr style="text-align:center"><td>修改的信息不能为空!</td></tr>
			<%} else{%>
			<tr>
				<td>公司</td>
				<td><input type="text" name="company" value=<%=work.getCompany() %> /></td>
			</tr>
			<tr>
				<td>开始时间</td>
				<td>
					<input type="text" name="start_time" onclick="laydate()" value=<%=work.getStartTime()%>/>
				</td>	
			</tr>
			<tr>
				<td>结束时间</td>
				<td>
					<input type="text" name="end_time" onclick="laydate()" value=<%=work.getEndTime()%>/>
				</td>
			</tr>
			<tr>
				<td>hr姓名</td>
				<td><input type="text" name="hr_name" value=<%=work.getHrName()%> /></td>
			</tr>
			<tr>
				<td>hr手机</td>
				<td><input type="text" name="hr_phone" value=<%=work.getHrPhone() %> /></td>
			</tr>
			<tr>
				<td>职位</td>
				<td><input type="text" name="position" value=<%=work.getPosition() %> /></td>
			</tr>
			<tr>
				<td>薪水</td>
				<td><input type="number" name="salary" value=<%=work.getSalary() %> /></td>
			</tr>
			<%} %>
		</table>
		<%if(message != "null") {%>
		<input type="submit" value="确定"/>
		<%} %>
		<a href="listResume"><input type="button" value="取消"></a>
	</form>
</body>
</html>