<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.dqm.pojo.Resume" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>修改简历</title>
</head>
<%Resume resume = (Resume)request.getAttribute("resume"); %>
<%String[] educations={"大专","学士","硕士","博士"};%>
<%String message = (String)request.getAttribute("message"); %>
<script type="text/javascript" src="js/laydate.dev.js"></script>	
	<script type="text/javascript">
		laydate({
			elem: '#start_time'
		});   
		laydate({
			elem: '#end_time'
		});		
	</script>
<body>
	<form action="updateResume1?id=
	<%if(message != "null"){%>
	<%=resume.getId() %><%} %>" method="post">
		<table width="100%" border=1>
			<%if(message == "null"){ %>
			<tr style="text-align:center"><td>修改的信息不能为空!</td></tr>
			<%} else{%>
			<tr>
				<td>姓名</td>
				<td><input type="text" name="name" value=<%=resume.getName() %> /></td>
			</tr>
			<tr>
				<td>性别</td>
				<td>
					<%if(resume.getSex()) {%>
					<input type="radio" name="sex" value="true" checked="checked"/>男&nbsp;
					<input type="radio" name="sex" value="false"/>女
					<%} else{%>
					<input type="radio" name="sex" value="true" />男&nbsp;
					<input type="radio" name="sex" value="false" checked="checked"/>女
					<%} %>
				</td>	
			</tr>
			<tr>
				<td>生日</td>
				<td><input type="text" name="birthday" onclick="laydate()" value=<%=resume.getBirthday()%>/>
				</td>
			</tr>
			<tr>
				<td>家庭住址</td>
				<td><textarea name="address"><%=resume.getAddress()%></textarea>
				</td>
			</tr>
			<tr>
				<td>民族</td>
				<td><textarea rows="3" cols="30" name="nation"><%=resume.getNation()%></textarea>
				</td>
			</tr>
			<tr>
				<td>现居地址</td>
				<td><textarea rows="3" cols="30" name="nowaddress"><%=resume.getNowaddress()%></textarea>
				</td>
			</tr>
			<tr>
				<td>政治面貌</td>
				<td><textarea rows="3" cols="30" name="political_status"><%=resume.getPoliticalStatus()%></textarea>
				</td>
			</tr>
			<tr>
				<td>职位</td>
				<td><textarea rows="3" cols="30" name="position"><%=resume.getPosition()%></textarea>
				</td>
			</tr>
			<tr>
				<td>专业</td>
				<td><textarea rows="3" cols="30" name="major"><%=resume.getMajor()%></textarea>
				</td>
			</tr>
			<tr>
				<td>最高学位</td>
				<td>
					<select name="education_level">
					<%for(int i = 0; i < 4; i++) {%>
						<%if(Integer.parseInt(resume.getEducationLevel()) == i) {%>
					<option value=<%=i%> selected="selected"><%=educations[i]%></option>
						<%} else {%>
							<option value=<%=i%>><%=educations[i]%></option>
						<%} %>
					<%} %>
					</select>
				</td>
			</tr>
			<%} %>
		</table>
		<%if(message != "null") {%>
		<input type="submit" value="确定"/>
		<%} %>
		<a href="listResume"><input type="button" value="取消"></a>
	</form>
</body>
</html>