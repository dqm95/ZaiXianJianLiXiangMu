<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>注册页面</title>
</head>
<% String message = (String)request.getAttribute("isExist"); %>
<body>
	<h2>欢迎光临君强科技有限公司简历注册！</h2>
	<form action="signup" method="post">
		用户名:<input type="text" name="username" placeholder="请输入用户名"/><br><br>
		<%if(message != null){ %>
			<span>用户名已存在!</span><br><br>
		<%} %>
		昵称:<input type="text" name="nickname" placeholder="请输入昵称"/><br><br>
		密码:<input type="password" name="password" placeholder="请输入密码"/><br><br>
		<input type="submit" value="注册">
	</form>
	
</body>
</html>