<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>添加工作经历</title>
<%String message = (String)request.getAttribute("message");%>
<script type="text/javascript" src="js/laydate.dev.js"></script>	
	<script type="text/javascript">
		laydate({
			elem: '#start_time'
		});   
		laydate({
			elem: '#end_time'
		});		
	</script>
<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script>
$(function(){  
	var a = 0;
    $("#getAtr").click(function(){  
    	var rows = document.getElementById("myTable").rows.length;
    	a++;
        $str='';
        $str+="<tr>";
        $str+="<td><input type='text' name=\"startTime"+a+"\" id=\"start_time"+a+"\" onClick='laydate()'/></td>";
        $str+="<td><input type='text' name=\"endTime"+a+"\" id=\"end_time"+a+"\" onClick='laydate()'/></td>";
        $str+="<td><input type='text' name=\"company"+a+"\" placeholder='请输入公司名称'/></td>";
        $str+="<td><input type='text' name=\"hr_name"+a+"\" placeholder='请输入hr姓名'/></td>";
        $str+="<td><input type='text' name=\"hr_phone"+a+"\" placeholder='请输入hr手机'/></td>";
        $str+="<td><input type='text' name=\"position"+a+"\" placeholder='请输入职位'/></td>";
        $str+="<td><input type='number' name=\"salary"+a+"\" placeholder='请输入薪水'/></td>";
        $str+="<td  onclick='getDel(this)'><a href='#'>删除</a></td>";   
        $str+="</tr>";
        $("#addTr").append($str);  
    });
});
 
function getDel(k){
    $(k).parent().remove();     
}
</script>
</head>
<body>
<div id="header">
	<%@include file="header.jsp" %>
	</div>
	<h2>欢迎光临君强科技有限公司简历注册！</h2>
	<form action="createWork" method="post">
	<%if(message == "null"){ %>
		<span style="font-size:x-large;">填写的信息不能为空!</span><br><br>
	<%} %>
		<table id="myTable" border="1">
			<tr>
				<td colspan="7" align="center"><h2>添加工作经历</h2></td>
				<td><input type="button" id="getAtr" value="Add"><input type="submit" value="complete"/></td>
			</tr>
			<tr>
				<td><center>开始时间</center></td>
				<td><center>结束时间</center></td>
				<td><center>公司</center></td>
				<td><center>hr姓名</center></td>
				<td><center>hr手机</center></td>
				<td><center>职位</center></td>
				<td><center>薪水</center></td>
				<td colspan="2"><center>操作</center></td>
			</tr>
			<tbody id="addTr">
			<tr>
				<td><input type="text" name="startTime0" onclick="laydate();"/></td>
				<td><input type="text" name="endTime0" onClick="laydate();"/></td>
				<td><input type="text" name="company0" placeholder="请输入公司名称"/></td>
				<td><input type="text" name="hr_name0" placeholder="请输入hr姓名"/></td>
				<td><input type="text" name="hr_phone0" placeholder="请输入hr手机"/></td>
				<td><input type="text" name="position0" placeholder="请输入职位"/></td>
				<td><input type="number" name="salary0" placeholder="请输入薪水"/></td>
				<td onclick="getDel(this)"><a href='#'>删除</a></td>
			</tr>	
			</tbody>
		</table>
	</form>

</body>
</html>