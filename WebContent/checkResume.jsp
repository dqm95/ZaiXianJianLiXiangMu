<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.util.List" %>
<%@ page import="com.dqm.pojo.Resume" %>
<%@ page import="com.dqm.pojo.User" %>
<%@ page import="com.dqm.pojo.EducationExperience" %>
<%@ page import="com.dqm.pojo.WorkExperience" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>查看简历</title>
</head>
<%Resume resume = (Resume)session.getAttribute("resume"); %>
<%List<EducationExperience> eduList = (List<EducationExperience>)session.getAttribute("eduList");%>
<%List<WorkExperience> workList = (List<WorkExperience>)session.getAttribute("workList");%>
<%User user = (User)session.getAttribute("currentUser"); %>
<body>
	<div id="header">
	<%@include file="header.jsp" %>
	</div>
	<form action="checkResume" method="post">
		<table	border="1px" cellpadding="0px" cellspacing="0px" width="1300px">
			<thead>
				<tr align="center" height="40px">
					<td>姓名</td>
					<td>性别</td>
					<td>生日</td>
					<td>住址</td>
					<td>民族</td>
					<td>现居地址</td>
					<td>政治面貌</td>
					<td>职位</td>
					<td>专业</td>
					<td>最高学位</td>
					<td>操作</td>
				</tr>
			</thead>
			<tbody>
				<tr align="center" height="40px">
					<td><%=resume.getName() %></td>
					<td>
						<%if(resume.getSex()){%>
						男
						<%}else{ %>
						女
						<%} %>
					</td>
					<td><%=resume.getBirthday() %></td>
					<td><%=resume.getAddress() %></td>
					<td><%=resume.getNation()%></td>
					<td><%=resume.getNowaddress() %></td>
					<td><%=resume.getPoliticalStatus() %></td>
					<td><%=resume.getPosition()%></td>
					<td><%=resume.getMajor() %></td>
					<td><%=resume.getEducationLevel() %></td>
					<td>
						<a href="updateResume?id=<%=resume.getId() %>"><input type="button" value="修改"/></a>
						<a href="listResume"><input type="button" value="返回"/></a>	
					</td>
				</tr>
			</tbody>
		</table>
		<br>
		<br>
		<table	border="1px" cellpadding="0px" cellspacing="0px" width="1300px">
			<thead>
				<tr align="center" height="40px">
					<td>学校</td>
					<td>开始时间</td>
					<td>结束时间</td>
					<td>取得学历</td>
					<td>操作</td>
				</tr>
			</thead>
			<tbody align="center" >
				<%for(EducationExperience education : eduList){%>
				<tr  height="40px">
					<td><%=education.getSchool() %></td>
					<td><%=education.getStartTime()%></td>
					<td><%=education.getEndTime() %></td>
					<td><%=education.getEducation() %></td>
					<td>
						<a href="updateEdu?id=<%=education.getId()%>"><input type="button" value="修改"/></a>
						<a href="deleteEdu?id=<%=education.getId()%>"><input type="button" value="删除"/></a>
						<a href="listResume"><input type="button" value="返回"/></a>	
					</td>
				</tr>
				<%} %>
			</tbody>
		</table>
		<br>
		<br>
		<table	border="1px" cellpadding="0px" cellspacing="0px" width="1300px">
			<thead>
				<tr align="center" height="40px">
					<td>公司</td>
					<td>开始时间</td>
					<td>结束时间</td>
					<td>hr姓名</td>
					<td>hr手机</td>
					<td>职位</td>
					<td>薪水</td>
					<td>操作</td>
				</tr>
			</thead>
			<tbody align="center">
				<%for(WorkExperience work : workList){%>
				<tr  height="40px">
					<td><%=work.getCompany() %></td>
					<td><%=work.getStartTime()%></td>
					<td><%=work.getEndTime() %></td>
					<td><%=work.getHrName() %></td>
					<td><%=work.getHrPhone() %></td>
					<td><%=work.getPosition() %></td>
					<td><%=work.getSalary() %></td>
					<td>
						<a href="updateWork?id=<%=work.getId()%>"><input type="button" value="修改"/></a>
						<a href="deleteWork?id=<%=work.getId()%>"><input type="button" value="删除"></a>
						<a href="listResume"><input type="button" value="返回"/></a>	
					</td>
				</tr>
				<%} %>
			</tbody>
		</table>
		<br>
	</form>
</body>
</html>