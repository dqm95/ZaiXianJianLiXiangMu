<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"  prefix="fmt"%>
<%@ page import="java.util.List" %>
<%@ page import="com.dqm.pojo.Resume" %>
<%@ page import="com.dqm.pojo.User" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>查询简历列表</title>
</head>
<%String message = (String)request.getAttribute("message"); %>
<%List<Resume> list = (List<Resume>)session.getAttribute("list"); %>
<%User user = (User)session.getAttribute("currentUser"); %>
<body> 
<div id="header">
	<%@include file="header.jsp" %>
</div>
<%if(message == "empty"){ %>
		<span>你目前还没有创建简历!</span><br><br>
<%} %>
<%if(message == "noEmpty"){ %>简历列表数量：<span><%=list.size()%></span>
<form action="listResume" method="post">
<table width="100%" border=1>
<tr>
	<td>用户名</td>
	<td>简历编号</td>
	<td>操作</td>
</tr>
		
	<%for(Resume r : list){ %>
	<tr>
	<td><%=user.getUsername() %></td>
	<td><%=r.getId() %></td>
	<td><a href="checkResume?id=<%=r.getId()%>"><input type="button" value="查看"/></a>
		<a href="delete?id=<%=r.getId()%>"><input type="button" value="删除"/></a>	</td>
		</tr>
	<%} %>
	
</table>
	<%} %>
</form>
</body>

</html>