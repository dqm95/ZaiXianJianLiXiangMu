package com.dqm.Until;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeUtil {

	public static Date parseTime(String dateTime){
		Date date = null;
		try {  
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");  
		    date = sdf.parse(dateTime);  
		} catch (ParseException e)  {  
		    System.out.println(e.getMessage());  
		}
		return date;
	}
}
