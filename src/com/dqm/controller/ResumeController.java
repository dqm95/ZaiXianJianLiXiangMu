package com.dqm.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.dqm.Until.TimeUtil;
import com.dqm.pojo.EducationExperience;
import com.dqm.pojo.Resume;
import com.dqm.pojo.User;
import com.dqm.pojo.WorkExperience;
import com.dqm.serviceImpl.EducationExperienceServiceImpl;
import com.dqm.serviceImpl.ResumeServiceImpl;
import com.dqm.serviceImpl.WorkExperienceServiceImpl;

@Controller
@RequestMapping
public class ResumeController {

	@Autowired
	private ResumeServiceImpl resumeServiceImpl;
	
	@Autowired
	private EducationExperienceServiceImpl educationExperienceServiceImpl;
	
	@Autowired
	private WorkExperienceServiceImpl workExperienceServiceImpl;
	
	@RequestMapping("/createResume")
	public String insertResume(HttpServletRequest request,Model model,HttpServletResponse response){
		String name = request.getParameter("name");
		String sex = request.getParameter("sex");
		String nation = request.getParameter("nation");
		String birthday = request.getParameter("birthday");
		String address = request.getParameter("address");
		String nowaddress = request.getParameter("nowaddress");
		String political_status = request.getParameter("political_status");
		String position = request.getParameter("position");
		String major = request.getParameter("major");
		String education_level = request.getParameter("education_level");
		
		if(name==""||sex == ""||birthday==""||address==""||nation==""||nowaddress==""||political_status==""||position==""||major==""||education_level==""){
			model.addAttribute("message","null");
			return "createResume";
		}
		
		Resume resume = new Resume();
		User user = (User)request.getSession().getAttribute("currentUser");
		resume.setUserId(user.getId());
		resume.setAddress(address);
		
		Date date = TimeUtil.parseTime(birthday);
		resume.setBirthday(date);
		
		resume.setEducationLevel(education_level);
		resume.setMajor(major);
		resume.setNowaddress(nowaddress);
		resume.setName(name);
		resume.setNation(nation);
		resume.setPosition(position);
		resume.setPoliticalStatus(political_status);
		resume.setSex(Boolean.parseBoolean(sex));
		
		resumeServiceImpl.insert(resume);
		request.getSession().setAttribute("resumeId", resume.getId());
		
		return "createEducation";
	}
	
	@RequestMapping("/listResume")
	public String listResume(HttpServletRequest request,Model model){
		User user = (User)request.getSession().getAttribute("currentUser");
		List<Resume> list = resumeServiceImpl.checkResumes(user.getId());
		
		if(list.isEmpty()){
			model.addAttribute("message", "empty");
			return "listResume";
		}
		model.addAttribute("message","noEmpty");
		request.getSession().setAttribute("list", list);
		return "listResume";
	}
	
	@RequestMapping("/delete")
	public String delete(HttpServletRequest request,String id,Model model){
		resumeServiceImpl.deleteByPrimaryKey(Integer.parseInt(id));
		User user = (User)request.getSession().getAttribute("currentUser");
		List<Resume> list = resumeServiceImpl.checkResumes(user.getId());
		if(list.isEmpty()){
			model.addAttribute("message", "empty");
			return "listResume";
		}
		model.addAttribute("message","noEmpty");
		request.getSession().setAttribute("list", list);
		return "listResume";
	}
	
	@RequestMapping("/checkResume")
	public String checkResume(String id,HttpServletRequest request,Model model){
		
		Resume resume = resumeServiceImpl.selectByPrimaryKey(Integer.parseInt(id));
		List<EducationExperience> eduList = educationExperienceServiceImpl.getEducations(Integer.parseInt(id));
		List<WorkExperience> workList = workExperienceServiceImpl.getWorkExperiences(Integer.parseInt(id));
	
		request.getSession().setAttribute("resume", resume);
		request.getSession().setAttribute("eduList", eduList);
		request.getSession().setAttribute("workList", workList);
		
		return "checkResume";
	}
	
	@RequestMapping("/updateResume")
	public String updateResume(String id,HttpServletRequest request,Model model){
		Resume resume = resumeServiceImpl.selectByPrimaryKey(Integer.parseInt(id));
		
		model.addAttribute("resume", resume);
		return "updateResume";
	}
	
	@RequestMapping("/updateResume1")
	public String updateResume1(String id,HttpServletRequest request,Model model){
		String name = request.getParameter("name");
		String sex = request.getParameter("sex");
		String nation = request.getParameter("nation");
		String birthday = request.getParameter("birthday");
		String address = request.getParameter("address");
		String nowaddress = request.getParameter("nowaddress");
		String political_status = request.getParameter("political_status");
		String position = request.getParameter("position");
		String major = request.getParameter("major");
		String education_level = request.getParameter("education_level");
		
		
		if(name==""||sex == ""||birthday==""||address==""||nation==""||nowaddress==""||political_status==""||position==""||major==""||education_level==""){
			model.addAttribute("message","null");
			return "updateResume";
		}
		
		Resume resume = new Resume();
		User user = (User)request.getSession().getAttribute("currentUser");
		resume.setId(Integer.parseInt(id));
		resume.setUserId(user.getId());
		resume.setAddress(address);
		
		Date date = TimeUtil.parseTime(birthday);
		resume.setBirthday(date);
		
		resume.setEducationLevel(education_level);
		resume.setMajor(major);
		resume.setNowaddress(nowaddress);
		resume.setName(name);
		resume.setNation(nation);
		resume.setPosition(position);
		resume.setPoliticalStatus(political_status);
		resume.setSex(Boolean.parseBoolean(sex));
		
		resumeServiceImpl.updateByPrimaryKey(resume);
		return "success";
	}
}
