package com.dqm.controller;

import java.math.BigDecimal;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.dqm.Until.TimeUtil;
import com.dqm.pojo.User;
import com.dqm.pojo.WorkExperience;
import com.dqm.serviceImpl.WorkExperienceServiceImpl;

@Controller
@RequestMapping
public class WorkExperienceController {

	@Autowired
	private WorkExperienceServiceImpl workExperienceServiceImpl;
	
	@RequestMapping("/createWork")
	public String createEducation(HttpServletRequest request,Model model){
		User user = (User)request.getSession().getAttribute("currentUser");
		int resumeId = (int)request.getSession().getAttribute("resumeId");
		
		
		for(int i = 0;;i++){
		
			if (request.getParameter("startTime" + i) != null && request.getParameter("endTime" + i) != null) {
			String company =request.getParameter("company" + i);
			String start_time =request.getParameter("startTime" + i);
			String end_time =request.getParameter("endTime" + i);
			String hr_name =request.getParameter("hr_name" + i);
			String hr_phone =request.getParameter("hr_phone" + i);
			String position =request.getParameter("position" + i);
			String salary =request.getParameter("salary" + i);
			System.out.println("==3=");
			if(company==""||hr_name == ""||hr_phone==""||position==""||salary==""){
				model.addAttribute("message","null");
				return "createWorkExperience";
			}
			
			Date startTime = TimeUtil.parseTime(start_time);
			Date endTime = TimeUtil.parseTime(end_time);
			

			WorkExperience workExperience = new WorkExperience();
			workExperience.setCompany(company);
			workExperience.setEndTime(endTime);
			workExperience.setHrName(hr_name);
			workExperience.setHrPhone(hr_phone);
			workExperience.setPosition(position);
			workExperience.setResumeId(resumeId);
			workExperience.setSalary(new BigDecimal(salary.trim()));
			workExperience.setStartTime(startTime);
			workExperience.setUserId(user.getId());
			System.out.println(workExperience);
			workExperienceServiceImpl.insert(workExperience);
			}else {
				break;
			}
		}
		return "success";
	}
	
	@RequestMapping("/updateWork")
	public String updateWork(String id,HttpServletRequest request, Model model){
		WorkExperience we = workExperienceServiceImpl.selectByPrimaryKey(Integer.parseInt(id));
		model.addAttribute("work", we);
		request.getSession().setAttribute("resumeId", we.getResumeId());
		return "updateWork";
	}
	
	@RequestMapping("/updateWork1")
	public String updateWork1(String id,HttpServletRequest request, Model model){
		User user = (User)request.getSession().getAttribute("currentUser");
		int resumeId = (int)request.getSession().getAttribute("resumeId");
		String company =request.getParameter("company");
		String start_time =request.getParameter("start_time");
		String end_time =request.getParameter("end_time");
		String hr_name =request.getParameter("hr_name");
		String hr_phone =request.getParameter("hr_phone");
		String position =request.getParameter("position");
		String salary =request.getParameter("salary");
		
		if(company==""||hr_name == ""||hr_phone==""||position==""||salary==""||end_time==""||start_time==""){
			model.addAttribute("message","null");
			return "updateWork";
		}
		
		Date startTime = TimeUtil.parseTime(start_time);
		Date endTime = TimeUtil.parseTime(end_time);
		

		WorkExperience workExperience = new WorkExperience();
		workExperience.setId(Integer.parseInt(id));
		workExperience.setCompany(company);
		workExperience.setEndTime(endTime);
		workExperience.setHrName(hr_name);
		workExperience.setHrPhone(hr_phone);
		workExperience.setPosition(position);
		workExperience.setResumeId(resumeId);
		workExperience.setSalary(new BigDecimal(salary.trim()));
		workExperience.setStartTime(startTime);
		workExperience.setUserId(user.getId());
	
		workExperienceServiceImpl.updateByPrimaryKey(workExperience);
		return "success";
	}
	
	@RequestMapping("/deleteWork")
	public String deleteWork(HttpServletRequest request,Model model,String id){
		workExperienceServiceImpl.deleteByPrimaryKey(Integer.parseInt(id));
		return "success";
	}
}
