package com.dqm.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.dqm.Until.TimeUtil;
import com.dqm.pojo.EducationExperience;
import com.dqm.pojo.User;
import com.dqm.serviceImpl.EducationExperienceServiceImpl;

@Controller
@RequestMapping
public class EducationExperienceController {

	@Autowired
	private EducationExperienceServiceImpl educationExperienceServiceImpl;

	@RequestMapping("/createEducation")
	public String createEducation(HttpServletRequest request, Model model) {
		User user = (User) request.getSession().getAttribute("currentUser");
		int resumeId = (int) request.getSession().getAttribute("resumeId");

		for (int i = 0;; i++) {
			if (request.getParameter("startTime" + i) != null && request.getParameter("endTime" + i) != null) {
				Date start = TimeUtil.parseTime(request.getParameter("startTime" + i));
				Date end = TimeUtil.parseTime(request.getParameter("endTime" + i));
				String school = request.getParameter("school" + i);
				String education = request.getParameter("education" + i);

				if (school == "" || education == "") {
					model.addAttribute("message", "null");
					return "createEducation";
				}

				EducationExperience ee = new EducationExperience();
				ee.setResumeId(resumeId);
				ee.setUserId(user.getId());
				ee.setStartTime(start);
				ee.setEndTime(end);
				ee.setSchool(school);
				ee.setEducation(education);
				educationExperienceServiceImpl.insert(ee);
			} else {
				break;
			}
		}
		return "createWorkExperience";

		/*
		 * String[] start_time = request.getParameterValues("start_time");
		 * String[] end_time = request.getParameterValues("end_time"); String[]
		 * school = request.getParameterValues("school"); String[] education =
		 * request.getParameterValues("education");
		 * 
		 * EducationExperience[] educations = new
		 * EducationExperience[school.length]; for(int i = 0; i<
		 * start_time.length; i++) { System.out.println(education[i]);
		 * System.out.println(start_time[i]); System.out.println(end_time[i]);
		 * System.out.println(school[i]);
		 * if(school[i]==""||start_time[i]==""||end_time[i]==""||education[i]==
		 * ""){ model.addAttribute("message", "null"); return "createEducation";
		 * }
		 * 
		 * Date startTime = TimeUtil.parseTime(start_time[i]); Date endTime =
		 * TimeUtil.parseTime(end_time[i]);
		 * 
		 * educations[i].setEducation(education[i]);
		 * educations[i].setEndTime(endTime);
		 * educations[i].setStartTime(startTime);
		 * educations[i].setSchool(school[i]);
		 * educations[i].setUserId(user.getId());
		 * educations[i].setResumeId(resumeId);
		 * 
		 * educationExperienceServiceImpl.insert(educations[i]); } return
		 * "createWorkExperience";
		 */
	}

	@RequestMapping("/deleteEdu")
	public String deleteEducation(String id,HttpServletRequest request,HttpServletResponse response) {
		educationExperienceServiceImpl.deleteByPrimaryKey(Integer.parseInt(id));
		return "success";
		
	}

	@RequestMapping("/updateEdu")
	public String updateEdu(String id, HttpServletRequest request, Model model) {
		EducationExperience ee = educationExperienceServiceImpl.selectByPrimaryKey(Integer.parseInt(id));
		model.addAttribute("edu", ee);
		request.getSession().setAttribute("resumeId", ee.getResumeId());
		return "updateEdu";
	}

	@RequestMapping("/updateEdu1")
	public String updateEdu1(String id, HttpServletRequest request, Model model) {
		User user = (User) request.getSession().getAttribute("currentUser");
		System.out.println("KKK"+request.getSession().getAttribute("resumeId"));
		int resumeId = (int) request.getSession().getAttribute("resumeId");

		Date start = TimeUtil.parseTime(request.getParameter("startTime"));
		Date end = TimeUtil.parseTime(request.getParameter("endTime"));
		String school = request.getParameter("school");
		String education = request.getParameter("education");

		if (school == "" || education == "") {
			model.addAttribute("message", "null");
			return "updateEdu";
		}

		EducationExperience ee = new EducationExperience();

		ee.setId(Integer.parseInt(id));
		ee.setResumeId(resumeId);
		ee.setUserId(user.getId());
		ee.setStartTime(start);
		ee.setEndTime(end);
		ee.setSchool(school);
		ee.setEducation(education);
		educationExperienceServiceImpl.updateByPrimaryKey(ee);

		return "success";

	}
}
