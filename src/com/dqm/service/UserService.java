package com.dqm.service;

import com.dqm.pojo.User;

public interface UserService {

	public void insertUser(User user);
	
	public User selectByPrimaryKey(Integer id);
	
	public boolean isLogin(User user);
	
	public boolean isExist(String username);
	
	public void updateByUsername(User user);
	
	public User selectByUser(User user);
}
