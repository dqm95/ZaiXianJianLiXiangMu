package com.dqm.service;

import java.util.List;

import com.dqm.pojo.EducationExperience;

public interface EducationExperienceService {

	int insert(EducationExperience record);
	
	List<EducationExperience> getEducations(Integer resumeId);
	
	int updateByPrimaryKey(EducationExperience record);
	
	EducationExperience selectByPrimaryKey(Integer id);
	
	int deleteByPrimaryKey(Integer id);
}
