package com.dqm.service;

import java.util.List;

import com.dqm.pojo.WorkExperience;

public interface WorkExperienceService {

	 int insert(WorkExperience record);
	 
	 List<WorkExperience> getWorkExperiences(Integer resumeId);
	
	 WorkExperience selectByPrimaryKey(Integer id);
	 
	 int updateByPrimaryKey(WorkExperience record);
	 
	 int deleteByPrimaryKey(Integer id);
}
