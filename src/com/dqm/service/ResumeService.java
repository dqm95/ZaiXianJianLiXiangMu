package com.dqm.service;

import java.util.List;

import com.dqm.pojo.Resume;

public interface ResumeService {

	public void insert(Resume resume);
	
	List<Resume> checkResumes(Integer userId); 
	
	void deleteByPrimaryKey(Integer id);
	
	Resume selectByPrimaryKey(Integer id);

	int updateByPrimaryKey(Resume record);
	
}
