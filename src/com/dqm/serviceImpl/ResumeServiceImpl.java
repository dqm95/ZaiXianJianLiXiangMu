package com.dqm.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dqm.dao.ResumeMapper;
import com.dqm.pojo.Resume;
import com.dqm.service.ResumeService;

@Service
public class ResumeServiceImpl implements ResumeService{

	@Autowired
	private ResumeMapper resumeMapper;
	
	@Override
	public void insert(Resume resume) {
		resumeMapper.insert(resume);
	}

	@Override
	public List<Resume> checkResumes(Integer UserId) {
		return resumeMapper.selectByUserId(UserId);
	}

	@Override
	public void deleteByPrimaryKey(Integer id) {
		resumeMapper.deleteByPrimaryKey(id);
	}

	@Override
	public Resume selectByPrimaryKey(Integer id) {
		return resumeMapper.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKey(Resume record) {
		return resumeMapper.updateByPrimaryKey(record);
	}
}
