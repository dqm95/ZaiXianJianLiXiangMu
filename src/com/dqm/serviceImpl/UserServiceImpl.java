package com.dqm.serviceImpl;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dqm.dao.UserMapper;
import com.dqm.pojo.User;
import com.dqm.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserMapper userMapper;
	@Override
	public void insertUser(User user) {
		 userMapper.insertSelective(user);
	}

	@Override
	public User selectByPrimaryKey(Integer id) {
		return userMapper.selectByPrimaryKey(id);
	}

	@Override
	public boolean isLogin(User u) {
		User user = userMapper.selectByUser(u);
		if(user == null) {
			return false;
		}
		return true;
	}

	@Override
	public boolean isExist(String username) {
		User user = userMapper.isExist(username);
		if(user == null) {
			return false;
		}
		return true;
	}

	@Override
	public void updateByUsername(User user) {
		userMapper.updateUser(user);
	}

	public static boolean isLoggedIn(HttpSession session) {
		if(session.getAttribute("currentUser") != null) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean isEmpty(User user){
		if(user.getUsername() == "" || user.getPassword() == "") {
			return true;
		}
		return false;
	}

	@Override
	public User selectByUser(User user) {
		return userMapper.selectByUser(user);
	}
	
}
