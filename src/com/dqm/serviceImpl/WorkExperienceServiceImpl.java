package com.dqm.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dqm.dao.WorkExperienceMapper;
import com.dqm.pojo.WorkExperience;

@Service
public class WorkExperienceServiceImpl implements com.dqm.service.WorkExperienceService {

	@Autowired
	private WorkExperienceMapper workExperienceMapper;
	@Override
	public int insert(WorkExperience record) {
		return workExperienceMapper.insert(record);
		
	}
	@Override
	public List<WorkExperience> getWorkExperiences(Integer resumeId) {
		return workExperienceMapper.getWorkExperiences(resumeId);
	}
	@Override
	public WorkExperience selectByPrimaryKey(Integer id) {
		
		return workExperienceMapper.selectByPrimaryKey(id);
	}
	@Override
	public int updateByPrimaryKey(WorkExperience record) {
		
		return workExperienceMapper.updateByPrimaryKey(record);
	}
	@Override
	public int deleteByPrimaryKey(Integer id) {
		
		return workExperienceMapper.deleteByPrimaryKey(id);
	}

}
