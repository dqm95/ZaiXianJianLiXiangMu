package com.dqm.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dqm.dao.EducationExperienceMapper;
import com.dqm.pojo.EducationExperience;

@Service
public class EducationExperienceServiceImpl implements com.dqm.service.EducationExperienceService {

	@Autowired
	private EducationExperienceMapper educationExperienceMapper;
	
	@Override
	public int insert(EducationExperience record) {
		return  educationExperienceMapper.insert(record);
	}

	@Override
	public List<EducationExperience> getEducations(Integer resumeId) {
		return educationExperienceMapper.getEducations(resumeId);
	}

	@Override
	public int updateByPrimaryKey(EducationExperience record) {
		return educationExperienceMapper.updateByPrimaryKey(record);
	}

	@Override
	public EducationExperience selectByPrimaryKey(Integer id) {
		return educationExperienceMapper.selectByPrimaryKey(id);
	}

	@Override
	public int deleteByPrimaryKey(Integer id) {
		return educationExperienceMapper.deleteByPrimaryKey(id);
	}

}
