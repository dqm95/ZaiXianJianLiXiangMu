package com.dqm.dao;

import java.util.List;

import com.dqm.pojo.EducationExperience;

public interface EducationExperienceMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(EducationExperience record);

    int insertSelective(EducationExperience record);

    EducationExperience selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(EducationExperience record);

    int updateByPrimaryKey(EducationExperience record);
    
    List<EducationExperience> getEducations(Integer resumeId);
}