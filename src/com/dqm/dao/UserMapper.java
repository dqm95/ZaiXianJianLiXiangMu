package com.dqm.dao;

import org.springframework.stereotype.Repository;

import com.dqm.pojo.User;

@Repository
public interface UserMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);
    
    User selectByUser(User user);
    
    User isExist(String username);
    
    void updateUser(User user);
}