package com.dqm.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.dqm.pojo.Resume;

public interface ResumeMapper {
    int deleteByPrimaryKey(@Param("id")Integer id);

    int insert(Resume record);

    int insertSelective(Resume record);

    Resume selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Resume record);

    int updateByPrimaryKey(Resume record);
    
    List<Resume> selectByUserId(Integer UserId);
    
}