package com.dqm.dao;

import java.util.List;

import com.dqm.pojo.WorkExperience;

public interface WorkExperienceMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(WorkExperience record);

    int insertSelective(WorkExperience record);

    WorkExperience selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(WorkExperience record);

    int updateByPrimaryKey(WorkExperience record);
    
    List<WorkExperience> getWorkExperiences(Integer resumeId);
}