package com.dqm.pojo;

import java.util.Date;

public class EducationExperience {
    @Override
	public String toString() {
		return "EducationExperience [id=" + id + ", userId=" + userId + ", resumeId=" + resumeId + ", school=" + school
				+ ", startTime=" + startTime + ", endTime=" + endTime + ", education=" + education + "]";
	}

	private Integer id;

    private Integer userId;

    private Integer resumeId;

    private String school;

    private Date startTime;

    private Date endTime;

    private String education;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getResumeId() {
        return resumeId;
    }

    public void setResumeId(Integer resumeId) {
        this.resumeId = resumeId;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school == null ? null : school.trim();
    }


    public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education == null ? null : education.trim();
    }
}