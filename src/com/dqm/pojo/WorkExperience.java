package com.dqm.pojo;

import java.math.BigDecimal;
import java.util.Date;

public class WorkExperience {
    @Override
	public String toString() {
		return "WorkExperience [id=" + id + ", resumeId=" + resumeId + ", userId=" + userId + ", company=" + company
				+ ", startTime=" + startTime + ", endTime=" + endTime + ", hrName=" + hrName + ", hrPhone=" + hrPhone
				+ ", position=" + position + ", salary=" + salary + "]";
	}

	private Integer id;

    private Integer resumeId;

    private Integer userId;

    private String company;

    private Date startTime;

    private Date endTime;

    private String hrName;

    private String hrPhone;

    private String position;

    private BigDecimal salary;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getResumeId() {
        return resumeId;
    }

    public void setResumeId(Integer resumeId) {
        this.resumeId = resumeId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company == null ? null : company.trim();
    }


    public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getHrName() {
        return hrName;
    }

    public void setHrName(String hrName) {
        this.hrName = hrName == null ? null : hrName.trim();
    }

    public String getHrPhone() {
        return hrPhone;
    }

    public void setHrPhone(String hrPhone) {
        this.hrPhone = hrPhone == null ? null : hrPhone.trim();
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position == null ? null : position.trim();
    }

    public BigDecimal getSalary() {
        return salary;
    }

    public void setSalary(BigDecimal salary) {
        this.salary = salary;
    }
}